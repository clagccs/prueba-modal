import React from 'react';
import ContentLogin from '../components/login/Contentlogin';

const LoginComponent = () => {
  return (
    <div className="login">
      <ContentLogin />
    </div>
  );
};

export default LoginComponent;
