import React from 'react';
import { connect } from 'react-redux';
import AuthenticationWrapper from '../components/shared/AuthenticationWrapper';
import SidebarSection from '../components/dashboard/SidebarSection/SidebarSection';
import BitaUpload from '../bitaComponents/BitaUpload/BitaUpload';
import * as CONSTANT from '../constants/app-constants';

const style = {
  home: 'w-12/12 m-auto',
  content: 'flex flex-wrap sm:flex-no-wrap',
};

const mapStateToProps = state => {
  return {
    state,
  };
};

const _UploadComponent = () => (
  <AuthenticationWrapper>
    <div className={style.content}>
      <div className="w-full sm:w-11/12 flex">
        <SidebarSection />
        <BitaUpload url={CONSTANT.UPLOAD_PATH} />
      </div>
    </div>
  </AuthenticationWrapper>
);

const Upload = connect(mapStateToProps)(_UploadComponent);
export default Upload;
