import React from 'react';
import AuthenticationWrapper from '../components/shared/AuthenticationWrapper';
import SidebarSection from '../components/dashboard/SidebarSection/SidebarSection';
import ListPanelSection from '../components/dashboard/DataSection/ListPanelSection/ListPanelSection';
import FilterPanel from '../components/dashboard/DataSection/FilterPanelSection/FilterPanel';
import ResultPanel from '../components/dashboard/DataSection/ResultPanel/ResultPanel';
import TitleSection from '../components/dashboard/DataSection/TitleSection/TitleSection';

const style = {
  home: 'w-12/12 m-auto',
  content: 'flex flex-wrap sm:flex-no-wrap',
};

const Dashboard = () => {
  return (
    <>
      <div className={style.content}>
        <div className="w-full  flex flex-wrap md:flex-no-wrap">
          <AuthenticationWrapper>
            <SidebarSection />
            <div className="w-full  flex flex-wrap ">
              <TitleSection />
              <ListPanelSection />
              <FilterPanel />
              <ResultPanel />
            </div>
          </AuthenticationWrapper>
        </div>
      </div>
    </>
  );
};

export default Dashboard;
