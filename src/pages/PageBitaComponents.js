import React from 'react';
import BitaUniverse from '../bitaComponents/BitaUniverse/BitaUniverse';
import BitaButton from '../bitaComponents/BitaButton/BitaButton';

const PruebaComponent = () => {
  return (
    <div>
      <BitaButton> Click me</BitaButton>
      <BitaButton primary> Click me</BitaButton>
      <BitaButton highlighted> Click me</BitaButton>

      <BitaUniverse small universe="market cap universe" title="bita women 20xa universe" />

      <BitaUniverse universe="market cap universe" title="bita women 20xa universe" />

      <BitaUniverse expanded universe="market cap universe" title="bita women 20xa universe" />
    </div>
  );
};

export default PruebaComponent;
