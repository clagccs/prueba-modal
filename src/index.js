import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import { animated } from 'react-spring';
import { Spring } from 'react-spring/renderprops';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  // useHistory,
  useLocation,
} from 'react-router-dom';
import { connect, Provider } from 'react-redux';
import store from './state/store/index';

const setModalBackgroundPageAction = _path => {
  return { type: 'SAVE_BACKGROUND', payload: _path };
};

const closeModalCleanBackgroudAction = () => ({ type: 'CLEAN_BACKGROUND' });

export default function ModalGalleryExample() {
  return (
    <Router>
      <ModalSwitch />
    </Router>
  );
}

const mapStateToProps = state => {
  return {
    backgroundPage: state.user.backgroundPage,
  };
};

function _ModalSwitch(props) {
  const location = useLocation();

  return (
    <div>
      <Switch location={props.backgroundPage || location}>
        <Route path="/" children={<Gallery />} />
      </Switch>

      {/* Show the modal when a background page is set */}

      {<Route exact path="/modal1" children={<Modal2 component={Hello} />} />}
      {<Route exact path="/modal2" children={<Modal2 component={Bye} />} />}
    </div>
  );
}

const ModalSwitch = connect(mapStateToProps)(_ModalSwitch);

function _LinkToModal({ path, background, children, dispatch }) {
  const saveBackground = () => {
    // only avoid when is undefined, when is false or null must run the dispatcher
    if (background !== undefined) dispatch(setModalBackgroundPageAction(background));
  };

  return (
    <Link
      to={{
        pathname: path,
      }}
      onClick={saveBackground}
    >
      {children}
    </Link>
  );
}

const LinkToModal = connect(mapStateToProps)(_LinkToModal);

function Gallery() {
  const location = useLocation();
  console.log('location inside Gallery ', location);

  return (
    <div>
      <div>
        <LinkToModal path="/modal1" background="/">
          hello Modal 1
        </LinkToModal>

        <div>
          <LinkToModal path="/modal2" background="/">
            hello Modal 2
          </LinkToModal>
        </div>
      </div>
    </div>
  );
}

function Hello() {
  return (
    <>
      <div>Hola</div>
      <div>
        <Link
          to={{
            pathname: '/modal2',
          }}
        >
          GO TO MODAL 2
        </Link>
      </div>
    </>
  );
}

function Bye() {
  return (
    <>
      <div>Modal2: Bye</div>
      <div>
        <Link
          to={{
            pathname: '/modal1',
          }}
        >
          GO TO MODAL 1
        </Link>
      </div>
    </>
  );
}

function _Modal2(props) {
  const closeModal = () => {
    console.log('closing');
    props.dispatch(closeModalCleanBackgroudAction());
  };

  const showModal = !!props.backgroundPage;

  return (
    <div>
      <Modal isOpen={showModal}>
        <button onClick={closeModal}>close</button>
        <Spring
          from={{ opacity: 0, transform: 'translate(-100%,0)' }}
          to={{ opacity: 1, transform: 'translate(0%,0)' }}
        >
          {_props => <animated.div style={_props}>{props.component()}</animated.div>}
        </Spring>
      </Modal>
    </div>
  );
}

const Modal2 = connect(mapStateToProps)(_Modal2);

ReactDOM.render(
  <Provider store={store}>
    <ModalGalleryExample />
  </Provider>,
  document.getElementById('root'),
);
