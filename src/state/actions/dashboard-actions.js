import * as ACTIONS from '../../constants/dashboard-actions';
import * as SB from '../../constants/siderbar-select';
import Axios from '../../Axios';

const storeLoadingIsActive = boolmustEnable => {
  return { type: ACTIONS.OPTION_IS_LOADING, payload: boolmustEnable };
};

export const getDashboardFromStorage = () => ({ type: ACTIONS.GET_DASHBOARD_FROM_STORAGE });

// guarda los usuarios en la store de redux
const storeUsers = users => {
  return { type: ACTIONS.STORE_USER, payload: users };
};

// guarda los indices. en la store de redux
const storeIndices = indices => {
  return { type: ACTIONS.STORE_INDICE, payload: indices };
};

// guarda los indices. en la store de redux
const storeCrypto = cryptos => {
  return { type: ACTIONS.STORE_CRYPTO, payload: cryptos };
};

// se ejecuta al clickear cualquiera de las opciones existentes en el sidebar
export const storeClickedSB = optionAction => {
  return { type: ACTIONS.CLICK_SIDEBAR_ICON, payload: optionAction };
};

// se ejecuta al clickear cualquiera de las opciones existentes en el sidebar
export const storeClickedOptionPanel = optionAction => {
  return { type: ACTIONS.STORE_SELECTED_OPTION_FROM_OPTION_PANEL, payload: optionAction };
};

export const storeSwitchState = payload => {
  return { type: ACTIONS.STORE_SWITCH_STATE, payload };
};

// se ejecuta cuando cambias el estado de uno de los switches en el filter panel, el payload es del tipo
// {selector : "OHLC", isActive : true}
export const storeSwitchChangeFilterPanel = payload => {
  return { type: ACTIONS.CHANGE_SWITCH_VALUE_FROM_OPTION_PANEL, payload };
};

export const storeChangeFilterDate = payload => {
  return { type: ACTIONS.CHANGE_DATE_FROM_OPTION_PANEL, payload };
};

export const storeDataTable = payload => {
  return { type: ACTIONS.STORE_DATA_TABLE, payload };
};

/**
 * BASADO EN EL ICONO SELECCIONADO DE SIDEBAR LAS OPCIONES MARCADAS Y FECHA REALIZA
 * EL REQUEST Y OBTIENE LA DATA PARA SER GUARDADA EN LA TABLA
 * @param {{date: *, activeOption: *, frecuency: boolean, sideBarSelected: *, id: *}} payload
 */
export const getData = payload => (dispatch, state) => {
  let url;
  let body;

  if (payload.sideBarSelected === SB.INDICES) {
    body = {
      indexes: [payload.id],
      start_date: payload.date.start.format('YYYY-MM-DD'),
      end_date: payload.date.end.format('YYYY-MM-DD'),
    };

    switch (payload.activeOption) {
      case 'OHLC':
        url = '/indexes_eod';
        break;
      case 'EOD':
        url = '/index_history';
        break;
      case 'TICK':
        url = '/index_tick';
        break;
      // no default
    }
  }

  if (payload.sideBarSelected === SB.CRYPTO) {
    body = {
      assets: [payload.id],
      start_date: payload.date.start.format('YYYY-MM-DD'),
      end_date: payload.date.end.format('YYYY-MM-DD'),
    };

    switch (payload.activeOption) {
      case 'OHLC':
        url = '/recorded_price_history';
        break;
      case 'EOD':
        url = '/recorded_prices_eod';
        break;
      case 'TICK':
        url = '/recorded_price_tick';
        break;
      // no default
    }
  }

  if (payload.frecuency) body.frecuency = payload.frecuency;

  Axios.post(url, body, { headers: { Authorization: `bearer ${state().user.token}` } }).then(
    rsp => {
      dispatch(storeDataTable(Object.values(rsp.data.data)[0]));
    },
  );
};

// cuando el usuario presiona usuario se muestra un spinner y se carga la data de la api
export const selectUsers = () => dispatch => {
  dispatch(storeLoadingIsActive(true));
  dispatch(storeClickedSB(SB.USERS));
  // esto simula una llamada a a la api
  setTimeout(() => {
    dispatch(storeLoadingIsActive(false));
    dispatch(
      storeUsers([
        { name: 'tom', email: 'tom@bitadata.com' },
        { name: 'ana', email: 'ana@bitadata.com' },
      ]),
    );
  }, 1500);
};

export const selectIndices = () => (dispatch, state) => {
  dispatch(storeLoadingIsActive(true));
  dispatch(storeClickedSB(SB.INDICES));
  Axios.get('/indexes', { headers: { Authorization: `bearer ${state().user.token}` } }).then(
    rsp => {
      dispatch(storeLoadingIsActive(false));
      console.log(rsp);
      dispatch(storeIndices(rsp.data.data));
    },
  );
};

export const selectCripto = () => (dispatch, state) => {
  dispatch(storeLoadingIsActive(true));
  dispatch(storeClickedSB(SB.CRYPTO));
  Axios.get('/indexes', { headers: { Authorization: `bearer ${state().user.token}` } })
    .then(rsp => {
      dispatch(storeLoadingIsActive(false));

      dispatch(storeCrypto(rsp.data.data));
    })
    .catch(err => {
      console.log('ERROR> ', err);
    });
};
