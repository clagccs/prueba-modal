export const setModalBackgroundPage = path => {
  const pathFormat = path && { pathname: path };
  return { type: 'SAVE_BACKGROUND', payload: pathFormat };
};
