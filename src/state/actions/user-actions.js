import * as ACTIONS from '../../constants/user-actions';
import Axios from '../../Axios';
import { LOCALSTORAGE_PATH } from '../../constants/app-constants';
import { LocalStorage } from '../../localStorage';

export const storeUser = user => {
  return { type: ACTIONS.STORE_USER, payload: user };
};

export const getUserFromStorage = () => ({ type: ACTIONS.GET_USER_FROM_LOCALSTORAGE });

export const storeToken = token => {
  return { type: ACTIONS.STORE_TOKEN, payload: token };
};

export const clearFromStorage = triedLoadFromStorage => {
  return { type: ACTIONS.STORE_STORAGE, payload: triedLoadFromStorage };
};

export const storeIsLoading = isLoading => {
  return { type: ACTIONS.USER_IS_LOADING, payload: isLoading };
};

export const storeError = error => {
  return { type: ACTIONS.USER_SET_ERROR, payload: error };
};

export const login = (user, password) => dispatch => {
  dispatch(storeIsLoading(true));
  const body = {
    user,
    password,
  };
  console.log(user, password);
  Axios.post('/login', body)
    .then(rsp => {
      console.log(rsp.data.message);
      const { token } = rsp.data;
      dispatch(storeToken(token));
      dispatch(storeUser(user));
    })
    .catch(err => {
      console.log(err);
      dispatch(storeError('Incorrect user or password'));
    });
};

export const storelogout = () => dispatch => {
  console.log('logout');
  dispatch(storeIsLoading(true));
  dispatch(storeToken(''));
  dispatch(storeUser(''));
  dispatch(clearFromStorage(false));
  LocalStorage.delete(LOCALSTORAGE_PATH);
};
