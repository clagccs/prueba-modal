import * as ACTIONS from '../../constants/dashboard-actions';
import * as SIDEBAR from '../../constants/siderbar-select';
import { LOCALSTORAGE_PATH } from '../../constants/app-constants';
import { LocalStorage } from '../../localStorage';

// función permite remover aquellas monedas que aparecen en B20 pero que ya existen en B10, o en B30 que aparecen en
// B20 o B10...debe recibir el arreglo de indexes ordenado tipo [B10,B20,B30] ya que va revisando en los índices
// sucesivos si la moneda existe
const filerDuplicateCoins = indexes => {
  const result = [];
  // mantiene una lista de las monedas que ya fueron indexadas
  let anexedCoins = [];

  // eslint-disable-next-line no-restricted-syntax
  for (const indice of indexes) {
    // revisa si la moneda ya fue idexada
    // eslint-disable-next-line no-loop-func
    const coins = indice.nodes.filter(coinData => !anexedCoins.includes(coinData.symbol));
    // actualiza el arreglo de monedas indexadas, anexedCoins es un arreglo de symbols
    anexedCoins = [...anexedCoins, ...coins.map(c => c.symbol)];
    indice.nodes = coins;
    result.push(indice);
  }

  return result;
};

const initialState = {
  sideBarSelected: SIDEBAR.NONE,
  optionLoading: false,
  optionPanelData: { data: null },
  filterPanelData: {
    display_filters: false,
    active: false,
    id: null,
    name: null,
    label: null,
    description: null,
    OHLC: true,
    EOD: false,
    TICK: false,
    date: null,
    frecuency: null,
  },
  tablePanelData: [],
};

function dashboardReducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.GET_DASHBOARD_FROM_STORAGE: {
      const storage = LocalStorage.load(LOCALSTORAGE_PATH);
      if (storage && storage.dashboard) {
        return storage.dashboard;
      }
      return state;
    }

    case ACTIONS.STORE_INDICE: {
      // necesitamos formatear la data que recibimos de la api o cuando cambia (usuario hace click en algun item)
      const dataFormatToTree = action.payload.map((indiceContent, id) => {
        const data = { ...indiceContent };
        data.key = id;
        data.label = `${data.name} (${data.index_id})`;
        data.name = `${data.name} `;
        return data;
      });
      return { ...state, optionPanelData: { data: dataFormatToTree } };
    }
    case ACTIONS.STORE_CRYPTO: {
      // necesitamos formatear la data que recibimos de la api o cuando cambia (usuario hace click en algun item)
      const dataFormatToTree = action.payload
        .map((indiceContent, id) => {
          const data = { ...indiceContent };
          data.key = id;
          data.index = data.index_id;
          data.label = `${data.name} (${data.index_id})`;
          data.name = `${data.name} `;
          if (data.components) {
            delete data.components;
            data.nodes = indiceContent.components.map((child, id2) => {
              const _child = { ...child };
              _child.key = `${id}-${id2}`;
              _child.label = `${child.name}(${child.symbol})`;
              return _child;
            });
          }
          return data;
        }) // solo mostramos lo de estos indices
        .filter(indice => ['B10', 'B20', 'B20XA', 'B30', 'B40', 'B50'].includes(indice.index_id));

      const cleanIndexes = filerDuplicateCoins(dataFormatToTree);

      return { ...state, optionPanelData: { data: cleanIndexes } };
    }

    case ACTIONS.STORE_DATA_TABLE: {
      return { ...state, tablePanelData: action.payload };
    }

    case ACTIONS.STORE_USER: {
      const dataFormatToTree = action.payload.map((indiceContent, id) => {
        const data = { ...indiceContent };
        data.key = id;
        data.label = `${data.name} (${data.email})`;
        return data;
      });
      return { ...state, optionPanelData: { data: dataFormatToTree } };
    }

    case ACTIONS.STORE_SELECTED_OPTION_FROM_OPTION_PANEL: {
      const filterData = { ...state.filterPanelData };
      filterData.label = action.payload.label;
      filterData.active = true;
      filterData.id = action.payload.index_id;
      filterData.description = action.payload.description;
      filterData.name = action.payload.name;

      //  en caso que se marque cryptos, tenemos que verificar si el usuario en las opciones marcó un indice
      //  o una crypto, los indices están en el level 0, las monedas están anidadas con level 1
      //  si es un indice solo mostramos la información en el area de filtro y no colocamos ni los selectores ni
      //  el calendario
      filterData.display_filters =
        state.sideBarSelected !== SIDEBAR.CRYPTO ||
        (state.sideBarSelected === SIDEBAR.CRYPTO && action.payload.level !== 0);

      return { ...state, filterPanelData: filterData };
    }

    case ACTIONS.CHANGE_DATE_FROM_OPTION_PANEL: {
      const filterData = { ...state.filterPanelData };
      filterData.date = action.payload;
      return { ...state, filterPanelData: filterData };
    }

    case ACTIONS.STORE_SWITCH_STATE: {
      const filterData = { ...state.filterPanelData, ...action.payload };

      return { ...state, filterPanelData: filterData };
    }

    case ACTIONS.CHANGE_SWITCH_VALUE_FROM_OPTION_PANEL: {
      const filterData = { ...state.filterPanelData };
      filterData[action.payload.selector] = action.payload.isActive;

      // deseleccionamos las otras opciones
      ['OHLC', 'EOD', 'TICK']
        .filter(op => op !== action.payload.selector)
        .forEach(op => {
          filterData[op] = false;
        });

      return { ...state, filterPanelData: filterData };
    }
    case ACTIONS.CLICK_SIDEBAR_ICON:
      return {
        ...initialState,
        sideBarSelected: action.payload,
      };

    case ACTIONS.OPTION_IS_LOADING:
      return { ...state, optionLoading: action.payload };
    default:
      return state;
  }
}

export default dashboardReducer;
