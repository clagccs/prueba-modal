import * as ACTIONS from '../../constants/user-actions';
import { LOCALSTORAGE_PATH } from '../../constants/app-constants';
import { LocalStorage } from '../../localStorage';

const initialState = {
  backgroundPage: null,
  username: '',
  token: '',
  loading: false,
  error: null,
  triedLoadFromStorage: false,
};

function userReducer(state = initialState, action) {
  switch (action.type) {
    case 'SAVE_BACKGROUND': {
      return { ...state, backgroundPage: action.payload };
    }

    case 'CLEAN_BACKGROUND': {
      return { ...state, backgroundPage: null };
    }

    case ACTIONS.GET_USER_FROM_LOCALSTORAGE: {
      const storage = LocalStorage.load(LOCALSTORAGE_PATH);
      if (storage && storage.user) {
        return { ...storage.user, triedLoadFromStorage: true };
      }
      return { ...state, triedLoadFromStorage: true };
    }

    case ACTIONS.STORE_STORAGE: {
      return { ...state, triedLoadFromStorage: action.payload };
    }

    case ACTIONS.STORE_DELETESTORAGE: {
      LocalStorage.delete(LOCALSTORAGE_PATH);
      return { ...state, triedLoadFromStorage: action.payload };
    }

    case ACTIONS.STORE_USER: {
      return { ...state, username: action.payload };
    }
    case ACTIONS.STORE_TOKEN: {
      return { ...state, token: action.payload };
    }

    case ACTIONS.USER_IS_LOADING: {
      return { ...state, loading: action.payload };
    }

    case ACTIONS.USER_SET_ERROR: {
      return { ...state, error: action.payload };
    }

    default:
      return state;
  }
}

export default userReducer;
