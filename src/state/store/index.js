import { applyMiddleware, createStore, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import dashboardReducer from '../reducers/dashboard-reducer';
import userReducer from '../reducers/user-reducer';
import { localStorageMiddleware } from '../localstoragemiddleware';

const store = createStore(
  combineReducers({ dashboard: dashboardReducer, user: userReducer }),
  composeWithDevTools(applyMiddleware(thunk, localStorageMiddleware)),
);
export default store;
