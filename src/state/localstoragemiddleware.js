import { LOCALSTORAGE_PATH } from '../constants/app-constants';
import { LocalStorage } from '../localStorage';

export const localStorageMiddleware = store => next => action => {
  next(action);
  // solo se comienza a guardar en el localstorage una vez el usuario está logueado
  if (store.getState().user.token) LocalStorage.save(LOCALSTORAGE_PATH, store.getState(), 500);
};
