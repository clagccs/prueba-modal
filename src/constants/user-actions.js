export const USER_IS_LOADING = 'USER/USER_IS_LOADING';
export const USER_SET_ERROR = 'USER/USER_SET_ERROR';
export const STORE_USER = 'USER/STORE_USER';
export const STORE_TOKEN = 'USER/STORE_TOKEN';
export const STORE_STORAGE = 'USER/STORE_STORAGE';
export const GET_USER_FROM_LOCALSTORAGE = 'USER/GET_USER_FROM_LOCALSTORAGE';
export const STORE_DELETESTORAGE = 'USER/STORE_DELETESTORAGE';
