export const CLICK_SIDEBAR_ICON = 'DASHBOARD/CLICK_SIDEBAR_ICON';
export const OPTION_IS_LOADING = 'DASHBOARD/OPTION_IS_LOADING';
export const STORE_USER = 'DASHBOARD/STORE_USER';
export const STORE_INDICE = 'DASHBOARD/STORE_INDICE';
export const STORE_CRYPTO = 'DASHBOARD/STORE_CRYPTO';
export const STORE_SELECTED_OPTION_FROM_OPTION_PANEL = 'DASHBOARD/STORE_SELECTED_OPTION';
export const STORE_SWITCH_STATE = 'DASHBOARD/STORE_SWITCH_STATE';
export const CHANGE_SWITCH_VALUE_FROM_OPTION_PANEL =
  'DASHBOARD/CHANGE_SWITCH_VALUE_FROM_OPTION_PANEL';
export const CHANGE_DATE_FROM_OPTION_PANEL = 'DASHBOARD/CHANGE_DATE_FROM_OPTION_PANEL';
export const STORE_DATA_TABLE = 'DASHBOARD/STORE_DATA_TABL';
export const GET_DASHBOARD_FROM_STORAGE = 'DASHBOARD/GET_DASHBOARD_FROM_STORAGE';
