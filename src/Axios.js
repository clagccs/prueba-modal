import * as axios from 'axios';

const Axios = axios.create({
  baseURL: 'https://api.bitadata.com/v1',
});

export default Axios;
