import React from 'react';
import PropTypes from 'prop-types';
import imagendefault from '../../img/Icons/internet.svg';
import style from './BitaUniverse.module.scss';

const BitaUniverse = props => {
  if (props.expanded && props.small) {
    throw new Error(
      'the bitaUniverse component must have a single size attribute expanded or small',
    );
  }
  // if pass expanded as a prop apply style.expanded
  // if pass smal as a prop appy style.small
  // in other case apply style normal
  // eslint-disable-next-line no-nested-ternary
  const bitaComponenteStyle = props.expanded
    ? style.expanded
    : props.small
    ? style.small
    : style.normal;

  return (
    <div className={bitaComponenteStyle}>
      <div>
        <img src={props.icon} alt={props.title} />
      </div>
      <div className="universe">{props.universe.toUpperCase()}</div>
      <div>
        <h1>{props.title.toUpperCase()}</h1>
      </div>
    </div>
  );
};

BitaUniverse.propTypes = {
  expanded: PropTypes.bool,
  small: PropTypes.bool,
  universe: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  icon: PropTypes.string,
};

BitaUniverse.defaultProps = {
  icon: imagendefault,
  expanded: false,
  small: false,
};

export default BitaUniverse;
