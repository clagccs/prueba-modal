import React from 'react';
import { mount } from 'enzyme';
import BitaUniverse from './BitaUniverse';
import imagen from './test_rsc/Icons/analysis.svg';

describe('BitaUniverse component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(
      <BitaUniverse
        icon={imagen}
        size="normal"
        universe="market cap universe"
        title="bita women 20xa universe"
      />,
    );
  });

  it('must render correctly the text Universe', () => {
    expect(wrapper.find('.universe').text()).toEqual('MARKET CAP UNIVERSE');
  });

  it('must render correctly the title', () => {
    expect(wrapper.find('h1').text()).toEqual('BITA WOMEN 20XA UNIVERSE');
  });

  it('must render correctly the images', () => {
    expect(wrapper.contains(<img src="analysis.svg" alt="bita women 20xa universe" />)).toEqual(
      true,
    );
  });
});
