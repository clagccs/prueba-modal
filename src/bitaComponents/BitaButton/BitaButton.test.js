import React from 'react';
import { mount } from 'enzyme';
import BitaButton from './BitaButton';

describe('BitaButton component', () => {
  let wrapper;
  let mockCallback;

  beforeEach(() => {
    mockCallback = jest.fn(x => x);

    wrapper = mount(<BitaButton onClick={mockCallback}>Click me</BitaButton>);
  });

  it('must render correctly the text in button', () => {
    expect(wrapper.find('.bitabutton').text()).toEqual('CLICK ME');
  });

  it("must run the callback passed to bitabutton when it's clicked", () => {
    wrapper
      .find('.bitabutton')
      .first()
      .simulate('click');
    expect(mockCallback.mock.calls.length).toEqual(1);
  });
});
