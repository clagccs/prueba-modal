import React from 'react';
import PropTypes from 'prop-types';
import style from './BitaButton.module.scss';

const BitaButton = props => {
  if (props.primary && props.highlighted) {
    throw new Error(
      'the bitaButton component must have a single style attribute primary or highlighted)',
    );
  }

  // if pass primary as a prop apply style.primary
  // if pass highlighted as a prop appy style.highlighted
  // in other case apply style default
  // eslint-disable-next-line no-nested-ternary
  const bitaComponenteStyle = props.primary
    ? style.primary
    : props.highlighted
    ? style.highlighted
    : style.default;

  return (
    <button
      type="button"
      onClick={props.onClick}
      className={`bitabutton ${bitaComponenteStyle}`}
      style={{ width: `${props.width}` }}
    >
      {props.children.toUpperCase()}
    </button>
  );
};

BitaButton.propTypes = {
  width: PropTypes.string,
  primary: PropTypes.bool,
  highlighted: PropTypes.bool,
};

BitaButton.defaultProps = {
  width: '25%',
  primary: false,
  highlighted: false,
};

export default BitaButton;
