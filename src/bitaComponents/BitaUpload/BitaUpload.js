import React from 'react';
import PropTypes from 'prop-types';
import 'react-dropzone-uploader/dist/styles.css';
import Dropzone from 'react-dropzone-uploader';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './BitaUpload.scss';

const BitaUpload = props => {
  let toastId = null;

  // specify upload params and url for your files
  const getUploadParams = ({ file }) => {
    const body = new FormData();
    body.append('uploadFile', file);
    return { url: props.url, body };
  };

  // called every time a file's `status` changes
  const handleChangeStatus = ({ meta, file }, status) => {
    console.log(status, meta, file);
    switch (status) {
      case 'preparing': {
        toastId = toast.info('preparing upload');
        break;
      }

      case 'uploading': {
        toast.update(toastId, { type: toast.TYPE.INFO, render: 'uploading...' });
        break;
      }

      case 'exception_upload': {
        toast.update(toastId, {
          type: toast.TYPE.ERROR,
          render: 'error uploading file, please try again',
        });
        break;
      }

      case 'done': {
        toast.update(toastId, {
          type: toast.TYPE.SUCCESS,
          autoClose: 3000,
          render: 'file was uploaded',
        });
        break;
      }

      case 'error_upload': {
        toast.update(toastId, {
          type: toast.TYPE.ERROR,
          autoClose: 3000,
          render: 'error trying to upload file, please try again',
        });
        break;
      }

      default: {
        console.log('status > ', status);
      }
    }
  };

  // receives array of files that are done uploading when submit button is clicked
  const handleSubmit = (files, allFiles) => {
    console.log(files.map(f => f.meta));
    allFiles.forEach(f => f.remove());
  };

  return (
    <div className="bitaUpload">
      <Dropzone
        getUploadParams={getUploadParams}
        onChangeStatus={handleChangeStatus}
        onSubmit={handleSubmit}
        accept="application/pdf,application/vnd.ms-excel"
        submitButtonContent="clear board"
      />
    </div>
  );
};

BitaUpload.propTypes = {
  url: PropTypes.string.isRequired,
};

export default BitaUpload;
