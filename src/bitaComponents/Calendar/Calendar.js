import React from 'react';
import DateRangePicker from 'react-daterange-picker';
import PropTypes from 'prop-types';
import './CalendarStyle.scss';
import styles from './Calendar.module.scss'; // Import css modules stylesheet as styles

const Calendar = props => {
  return (
    <div>
      <p className={styles.calendartitle}>{props.title}</p>
      <div className={styles.contenteCalendar}>
        <DateRangePicker onSelect={props.select} value={props.fecha} />
      </div>
    </div>
  );
};

Calendar.propTypes = {
  select: PropTypes.string.isRequired,
  fecha: PropTypes.string.isRequired,
  title: PropTypes.string,
};

Calendar.defaultProps = {
  title: 'Select Date',
};

export default Calendar;
