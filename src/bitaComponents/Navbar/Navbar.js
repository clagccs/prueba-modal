import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import style from './Navbar.module.scss';
import User from './User';
import * as Action from '../../state/actions/user-actions';
import logout from '../../img/Icons/logout.svg';

const mapStateToProps = state => {
  return { username: state.user.username };
};

const NavbarContentComponent = props => {
  const clickLogout = () => {
    props.dispatch(Action.storelogout('logout'));
  };

  return (
    <div className={style.navbar}>
      <div>
        <strong>BITA</strong> Dashboard
      </div>
      <div className={style.contentflex}>
        <User />
        <div className={style.contentLogout}>
          <img src={logout} alt="Logo" className={style.icon} />
          <button onClick={clickLogout}>Log out</button>
        </div>
      </div>
    </div>
  );
};

const NavbarComponent = withRouter(connect(mapStateToProps)(NavbarContentComponent));
export default NavbarComponent;
