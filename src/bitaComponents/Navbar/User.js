import React from 'react';
import style from './Navbar.module.scss';

const UserComponent = () => {
  const nombreUser = 'Fulvia Buonanno';
  const correoUser = 'fb@bitada.com';

  return (
    <div className={style.contentUser}>
      <img className={style.avatar} alt="." />
      <div>
        <div className={style.nameUser}>{nombreUser}</div>
        <div>{correoUser}</div>
      </div>
    </div>
  );
};

export default UserComponent;
