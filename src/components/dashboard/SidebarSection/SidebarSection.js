import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import * as Action from '../../../state/actions/dashboard-actions';
import Sidebar from '../../../bitaComponents/Sidebar/Sidebar';

// Imagenes del sidebar
import index from '../../../img/Icons/global-network.svg';
import users from '../../../img/Icons/analysis.svg';
import cryptos from '../../../img/Icons/internet.svg';
import report from '../../../img/Icons/report.svg';

const mapStateToProps = state => {
  return { sideBarSelected: state.dashboard.sideBarSelected };
};

const SidebarSectionComponent = props => {
  return (
    <>
      <Sidebar>
        <Sidebar.Button
          label="Index"
          image={index}
          onClick={() => {
            props.history.push('/dashboard');
            props.dispatch(Action.selectIndices());
          }}
        />
        <Sidebar.Button
          label="Cryptos"
          image={cryptos}
          onClick={() => {
            props.history.push('/dashboard');
            props.dispatch(Action.selectCripto());
          }}
        />
        <Sidebar.Button
          label="User"
          image={users}
          onClick={() => {
            props.history.push('/dashboard');
            props.dispatch(Action.selectUsers());
          }}
        />
        <Sidebar.Button
          label="Upload File"
          image={report}
          onClick={() => props.history.push('/upload')}
        />
      </Sidebar>
    </>
  );
};

const SidebarSection = withRouter(connect(mapStateToProps)(SidebarSectionComponent));
export default SidebarSection;
