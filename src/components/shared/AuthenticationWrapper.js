// este componente hace un wrap (engloba) a otro componente, en caso del usuario
// estar logueado se ejecuta el componente interno, si no hace el redireccionamiento
// a login

import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import * as UserActions from '../../state/actions/user-actions';
import * as DashboardActions from '../../state/actions/dashboard-actions';

const mapStateToProps = state => {
  return {
    token: state.user.token,
    tried: state.user.triedLoadFromStorage,
  };
};

const _AuthenticationWrapper = props => {
  // si el usuario no está logueado
  if (!props.token) {
    // ...y no hemos intentado cargarlo del localStorage
    // intentamos cargarlo del localStorage
    if (!props.tried) {
      props.dispatch(DashboardActions.getDashboardFromStorage());
      props.dispatch(UserActions.getUserFromStorage());
    } else {
      // si no conseguimos las credenciales lo redireccionamos a login
      props.history.push('/login');
    }
  }

  // en caso que si tenga su token<
  return <>{props.children}</>;
};

const AuthenticationWrapper = withRouter(connect(mapStateToProps)(_AuthenticationWrapper));
export default AuthenticationWrapper;
