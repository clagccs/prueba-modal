import React from 'react';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const mapStateToProps = state => {
  return { error: state.user.error };
};

toast.configure();

const MensajeErrorComponent = props => {
  if (props.error) {
    toast.dismiss();
    toast(props.error, {
      position: 'bottom-center',
      hideProgressBar: true,
      autoClose: 4000,
    });
  }
  const component = !props.error ? <></> : <h1>{props.error}</h1>;
  return <div>{component}</div>;
};

const MensajeError = connect(mapStateToProps)(MensajeErrorComponent);
export default MensajeError;
