import React from 'react';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';
import styles from './Contentlogin.module.scss';
import * as Action from '../../state/actions/user-actions';
import Bita from '../../img/Icons/BITA.svg';

const mapStateToProps = state => {
  return { username: state.user.username, error: state.user.error };
};

toast.configure();

const ContentLoginComponent = props => {
  const userInput = React.createRef();
  const passwordInput = React.createRef();

  if (props.error) {
    toast.dismiss();
    toast(props.error, {
      position: 'bottom-center',
      hideProgressBar: true,
      autoClose: 4000,
    });
    props.dispatch(Action.storeError(null));
  }

  const clickLogin = () => {
    toast('Loading...', {
      position: 'bottom-center',
      hideProgressBar: true,
      autoClose: 4000,
    });
    const user = userInput.current.value;
    const password = passwordInput.current.value;
    props.dispatch(Action.login(user, password));
    console.log(props.error);
  };

  const component = props.username ? (
    <Redirect to="/" />
  ) : (
    <div className="flex flex-col  items-center ">
      <img src={Bita} alt={props.label} className={styles.img} />
      <input
        type="text"
        name="user"
        placeholder="EMAIL OR USERNAME"
        aria-label="Full name"
        ref={userInput}
      />
      <input type="password" name="PASSWORD" placeholder="PASSWORD" ref={passwordInput} />
      <span>Forgot</span>
      <div className="flex ">
        {' '}
        <input type="checkbox" id="remenbe" /> Remember me
      </div>
      <button onClick={clickLogin}>LOGIN</button>
      {/* eslint-disable-next-line react/no-unescaped-entities */}
      <p>Don't have a BITACore account? Contact our sales team</p>
      <p>to discover our license pricing</p>
      <p className="text-azulelectrico">bitacore@bitada.com</p>
    </div>
  );

  return <div className={styles.contentlogin}>{component}</div>;
};

const ContentLogin = connect(mapStateToProps)(ContentLoginComponent);
export default ContentLogin;
