module.exports = {
  important: true,
  theme: {
    colors: {
      indigo: '#5c6ac4',
      blue: '#007ace',
      red: '#de3618',
      azulelectrico: '#50C1FB',
    },
    extend: {},
  },
  variants: {},
  plugins: [],
};
